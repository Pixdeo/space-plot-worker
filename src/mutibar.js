import cliProgress from 'cli-progress'

export const multibar = new cliProgress.MultiBar(
  {
    format: 'Plotter: {plotter} |' + ('{bar}') + '| ID: {id} | Phase: {phase} | {percentage}% | ETA: {eta}',
    clearOnComplete: false,
    hideCursor: true,
    etaBuffer: 500
  },
  cliProgress.Presets.rect
)
