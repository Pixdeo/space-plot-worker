export const State = {
  idle: 0,
  plotting: 1
}

function increment() {
  jobCount += 1
}

function decrement() {
  jobCount -= 1

  if (jobCount < 0) {
    jobCount = 0
  }
}

export var jobCount = 0

jobCount.__proto__.increment = increment
jobCount.__proto__.decrement = decrement
