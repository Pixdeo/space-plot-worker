import { plotCreate } from './plotCreate/index.js'
import c from 'chalk'

// Task
export async function plot(job) {
  const { poolKey, farmerKey } = job
  console.log(
    `${c.yellow('job:')} Starting plot job ${job.id} with ${farmerKey}`
  )
  return await plotCreate(poolKey, farmerKey)
}
