import { logger } from '../../logger.js'
import { multibar } from '../../mutibar.js'
import { statusJob } from '../../api/index.js'

import { State } from './State.js'

const defaultBuckets = 128
export var path = ''

const state = new State()

export class Parser {
  constructor(jobId, config) {
    // add information of the job
    this.jobId = jobId
    this.buckets = defaultBuckets
    this.state = new State(config.plotter)
    this.progressbar = multibar.create(100, 0, {id: jobId, plotter: config.plotter})
    this.config = config
    this.completionPrior = 0
    // this.progressbar.start(100, 0)
  }

  async parse(data) {
    var completion = this.state.completed()
    // console.log(`${data}`)
    // await new Promise((r) => setTimeout(r), 1500)

    // detect current phase change, setup wait for the next phase
    const phaseChange = `${data}`.match(`${this.state.phaseLine()}`)
    if (phaseChange) {
      logger(20, "parse.js - matched phase: " + this.state.phaseLine())
      if (this.state.isPath()) {
        path = phaseChange[1]
        if (this.config.plotter === 'madmax') {
          path = this.config.customerDir + path + ".plot"
        }
        logger(10, 'parse.js - path found: ' + path)
      }

      this.state.next()
      // process.stdout.write(`${this.state.phaseLine()} | done: ${completion}%\r`)
      this.progressbar.update(parseInt(completion), { phase: this.state.getPhase() })
      statusJob(this.jobId, parseInt(completion), this.state.getPhase(), this.progressbar.eta.getTime())
      return
    }

    // check if this is a first computation Pass
    // this is done later on the Compressing Phase
    if (`${data}`.match('First computation pass')) {
      this.state.currentSubPhase = true
    }

    let bucket = /Bucket (\d+)/gm.exec(`${data}`)
    if (bucket) {
      const partial = bucket[1]

      // step percent size to increment between phases
      var step
      var doubleBucket

      if (this.state.isRepeating()) {
        // first phase
        step =
          (this.state.nextCompleted() - this.state.completed()) /
          this.buckets /
          2
        if (!this.state.currentSubPhase) {
          doubleBucket = 0
        } else {
          doubleBucket =
            (this.state.nextCompleted() - this.state.completed()) / 2
        }
        // second phase
      } else {
        step =
          (this.state.nextCompleted() - this.state.completed()) / this.buckets
        doubleBucket = 0
      }

      // console.log(
      //   `Plot: repeating: ${this.state.isRepeating()}, currentSubPhase: ${
      //     this.state.currentSubPhase
      //   }, step: ${step}, doubleBucket: ${doubleBucket}`,
      // )

      const from = this.state.completed() + doubleBucket
      completion = (from + partial * step).toFixed(2)
      logger(20, parseInt(completion) + " " + parseInt(this.completionPrior))
      if (parseInt(completion) - parseInt(this.completionPrior) >= 1) {
        statusJob(this.jobId, parseInt(completion), this.state.getPhase(), this.progressbar.eta.getTime())
      }
      this.completionPrior = completion
    }

    // process.stdout.write(`Plot: ${this.state.phaseLine()} | done: ${completion}%\r`)
    this.progressbar.update(parseInt(completion), { filename: this.jobId })
    // console.log(completion)
  }
}
