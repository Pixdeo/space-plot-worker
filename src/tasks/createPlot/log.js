'use strict'

import spawn from 'await-spawn'
import { Parser, path } from './parse.js'

export async function plotCreate(poolKey, farmerKey) {
  console.info(`Fake Plot Creating (using log)`)

  //
  const parser = new Parser()

  let main = process.env.PWD
  const chiaPlot = spawn('cat', [
    `${main}/log/plot5_2021-05-25_09_08_16_807620.log`
  ])

  console.log('hhh')
  // await chiaPlot.child.stdout.on('data', (data) => )
  await chiaPlot.child.stdout.on('data', async (data) => {
    // let lines = `${data}`.split('\n')
    // console.log('inicio', lines)
    // for await (const line of lines) {
    //   await parser.parse(line)
    // }
    console.log('*** aca ***')
  })

  // chiaPlot.child.stderr.on('data', (data) => {
  //   console.error(`${data}`)
  // })

  await chiaPlot.child.on('close', async (code) => {
    console.log('done')
    // console.log(`child process exited with code ${code}`)
    // console.log('final', path)
  })

  process.on('SIGINT', function () {
    console.log('Caught interrupt signal')

    chiaPlot.kill(9)
    // process.exit()
  })

  await chiaPlot
  return path
}
