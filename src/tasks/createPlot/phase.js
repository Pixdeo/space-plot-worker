export const phaseMap = {
  'Computing table 1': { phase: 1, at: 1, isRepeating: false },
  'Computing table 2': { phase: 1, at: 6, isRepeating: false },
  'Computing table 3': { phase: 1, at: 12, isRepeating: false },
  'Computing table 4': { phase: 1, at: 20, isRepeating: false },
  'Computing table 5': { phase: 1, at: 28, isRepeating: false },
  'Computing table 6': { phase: 1, at: 36, isRepeating: false },
  'Computing table 7': { phase: 2, at: 42, isRepeating: false },

  //
  'Backpropagating on table 7': { phase: 2, at: 43, isRepeating: false },
  'Backpropagating on table 6': { phase: 2, at: 48, isRepeating: false },
  'Backpropagating on table 5': { phase: 2, at: 51, isRepeating: false },
  'Backpropagating on table 4': { phase: 2, at: 55, isRepeating: false },
  'Backpropagating on table 3': { phase: 2, at: 58, isRepeating: false },
  'Backpropagating on table 2': { phase: 3, at: 61, isRepeating: false },

  // repeats the calc
  'Compressing tables 1 and 2': { phase: 3, at: 66, isRepeating: true },
  'Compressing tables 2 and 3': { phase: 3, at: 73, isRepeating: true },
  'Compressing tables 3 and 4': { phase: 3, at: 79, isRepeating: true },
  'Compressing tables 4 and 5': { phase: 3, at: 85, isRepeating: true },
  'Compressing tables 5 and 6': { phase: 3, at: 92, isRepeating: true },
  'Compressing tables 6 and 7': { phase: 4, at: 98, isRepeating: true },

  'Write Checkpoint tables': { phase: 5, at: 99, isRepeating: false },

  'Renamed final file.*to."(.*)"': { phase: 5, at: 100, isRepeating: false, isPath: true }
}

export const phaseMapMadMax = {
  // *************
  // madmax phases
  // *************
  'Plot Name: (.*)': { phase: 1, at: 0, isRepeating: false, isPath: true },
  '.P1. Table 1.*': { phase: 1, at: 1, isRepeating: false },
  '.P1. Table 2.*': { phase: 1, at: 7, isRepeating: false },
  '.P1. Table 3.*': { phase: 1, at: 15, isRepeating: false },
  '.P1. Table 4.*': { phase: 1, at: 24, isRepeating: false },
  '.P1. Table 5.*': { phase: 1, at: 31, isRepeating: false },
  '.P1. Table 6.*': { phase: 1, at: 38, isRepeating: false },
  '.P1. Table 7.*': { phase: 2, at: 45, isRepeating: false },
  '.P2. Table 7 rewrite.*': { phase: 2, at: 49, isRepeating: false },
  '.P2. Table 6 rewrite.*': { phase: 2, at: 53, isRepeating: false },
  '.P2. Table 5 rewrite.*': { phase: 2, at: 57, isRepeating: false },
  '.P2. Table 4 rewrite.*': { phase: 2, at: 61, isRepeating: false },
  '.P2. Table 3 rewrite.*': { phase: 2, at: 65, isRepeating: false },
  '.P2. Table 2 rewrite.*': { phase: 2, at: 69, isRepeating: false },
  '.P2. Table 2 rewrite.*': { phase: 3, at: 69, isRepeating: false },
  '.P3-1. Table 2.*': { phase: 3, at: 71, isRepeating: false },
  '.P3-2. Table 2.*': { phase: 3, at: 73, isRepeating: false },
  '.P3-1. Table 3.*': { phase: 3, at: 75, isRepeating: false },
  '.P3-2. Table 3.*': { phase: 3, at: 77, isRepeating: false },
  '.P3-1. Table 4.*': { phase: 3, at: 79, isRepeating: false },
  '.P3-2. Table 4.*': { phase: 3, at: 81, isRepeating: false },
  '.P3-1. Table 5.*': { phase: 3, at: 83, isRepeating: false },
  '.P3-2. Table 5.*': { phase: 3, at: 85, isRepeating: false },
  '.P3-1. Table 6.*': { phase: 3, at: 87, isRepeating: false },
  '.P3-2. Table 6.*': { phase: 3, at: 89, isRepeating: false },
  '.P3-1. Table 7.*': { phase: 3, at: 91, isRepeating: false },
  '.P3-2. Table 7.*': { phase: 3, at: 93, isRepeating: false },
  '.P4. Starting.*': { phase: 4, at: 95, isRepeating: false },
  '.P4. Finished writing C1.*': { phase: 4, at: 96, isRepeating: false },
  '.P4. Writing.*': { phase: 4, at: 97, isRepeating: false },
  'Phase 4 took.*': { phase: 5, at: 98, isRepeating: false },
  'Copy to.*': { phase: 5, at: 100, isRepeating: false }
}
