import { phaseMap, phaseMapMadMax } from './phase.js'

export class State {
  constructor(plotter) {
    this.phaseMap = phaseMap
    if (plotter === 'madmax') {
      // console.log("using madmax phase map")
      this.phaseMap = phaseMapMadMax
    }
    this.currentPhase = 0
    this.currentSubPhase = false
  }

  phaseLine() {
    return Object.keys(this.phaseMap)[this.currentPhase]
  }

  isRepeating() {
    let key = this.phaseLine()
    return this.phaseMap[key].isRepeating
  }

  nextCompleted() {
    if (this.currentPhase == Object.keys(this.phaseMap).length - 1) {
      return 100
    }

    let pos = Object.keys(this.phaseMap)[this.currentPhase + 1]
    return this.phaseMap[pos].at
  }

  getPhase() {
    if (this.currentPhase >= Object.keys(this.phaseMap).length - 1) {
      return 100
    }

    let key = this.phaseLine()
    return this.phaseMap[key].phase
  }

  completed() {
    if (this.currentPhase >= Object.keys(this.phaseMap).length - 1) {
      return 100
    }

    let key = this.phaseLine()
    return this.phaseMap[key].at
//    return this.phaseMap[this.currentPhase].at
  }

  next() {
    // console.log('next', this.currentPhase, Object.keys(phase).length)
    if (this.currentPhase >= Object.keys(this.phaseMap).length - 1) {
      return
    }

    this.currentPhase += 1
    this.currentSubPhase = false
  }

  isPath() {
    let key = this.phaseLine()
    if ('isPath' in this.phaseMap[key] && this.phaseMap[key].isPath) {
      return true
    }
    return false
  }
}
