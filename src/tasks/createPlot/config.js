// This is a bit redundant, but serves to provide default values
export function makePlotConfig({buffer=3420, tempDir="tmp", finalDir="plots/final", kSize=32, threads=2, plotter="chia"}) {
  // customerDir is a changed per plot job
  return {
    customerDir: "",
    plotter: plotter,
    buffer: buffer,
    tempDir: tempDir,
    finalDir: finalDir,
    kSize: kSize,
    threads: threads
  }
}
