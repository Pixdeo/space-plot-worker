'use strict'

// Plot Config
import { DEBUG, chiaPathExec, madmaxExec } from '../../../worker-config.js'
import fs from 'fs'
import spawn from 'await-spawn'

import { Parser, path } from './parse.js'
import { multibar } from '../../mutibar.js'

function setCustomerDir (c, custId) {
  c.customerDir = c.finalDir + "/" + custId + "/"
}

export function configToParams (c) {
  var retval = []

  // support for madmax plotter
  if (c.plotter === 'madmax') {
    retval = ['-t', c.tempDir, '-d', c.customerDir, '-r', c.threads]
  } else {
    retval = [
      'plots', 'create',
      '-b', c.buffer, '-t', c.tempDir, '-d', c.customerDir, '-k', c.kSize, '-r', c.threads
    ]
    if (c.kSize < 32) {
      retval.push('--override-k')
    }
  }
  if (('temp2Dir' in c) && (c.temp2Dir.len() > 0)) {
    retval.push('-2', c.temp2Dir)
  }
  return retval
}

// mockPlot simply creates a progressbar that progresses to 100 over 10 seconds
export async function mockPlot({ jobId, plotConfig, poolKey, farmerKey }) {
  const custId = farmerKey.substr(0,7)  // until we pass custId through API
  setCustomerDir(plotConfig, custId)
  const params = [
    'plots', 'create',
    '-p', poolKey, '-f', farmerKey,
    ...configToParams(plotConfig),
  ]

  console.log(`Mock Job ${jobId}, using config: ${params}`)
  var progressbar = multibar.create(100, 0, {id: jobId, plotter: "mock"})
  for (let step = 0; step <= 100; step++) {
    //console.log("step is " + step)
    let phase = parseInt(step/20)
    let eta = progressbar.eta.getTime()
    // console.log(`eta is ${eta}`)
    progressbar.update(step, {phase: phase})
    if (step == 100) {
      progressbar.stop()
      multibar.remove(progressbar)
      return "mockPlot_path_for_job_" + jobId
    }
    await new Promise(resolve => setTimeout(resolve, 100));
  }
}

// Plot Create, call Chia plot create and parse output
// returns the url
export async function createPlot({ jobId, plotConfig, poolKey, farmerKey }) {
  const custId = farmerKey.substr(0,7)  // until we pass custId through API
  setCustomerDir(plotConfig, custId)
  const params = [
    ...configToParams(plotConfig),
    '-p', poolKey, '-f', farmerKey,
  ]

  console.log(`Starting Job ${jobId}, using config: ${params}`)

  try {

    let exec = chiaPathExec
    if (plotConfig.plotter === 'madmax') {
      exec = madmaxExec
      // madmax does not auto create directories
      let customerDir = plotConfig.customerDir
      if (!fs.existsSync(customerDir)){
          fs.mkdirSync(customerDir);
      }
    }
    const chiaPlot = spawn(exec, params)

    const parser = new Parser(jobId, plotConfig)

    chiaPlot.child.stdout.on('data', (data) => {
      if (DEBUG > 20) {
        console.log(`${data}`)
      }
      parser.parse(data)
    })

    chiaPlot.child.stderr.on('data', (data) => {
      if (DEBUG > 20) {
        console.error(`error: ${data}`)
      }
      parser.parse(data)
    })

    console.log(path)

    // chiaPlot.on('close', (code) => {
    //   console.log(`child process exited with code ${code}`)
    // })

    await chiaPlot
    return path
  } catch (e) {
    console.log(`${e}`)
  }
}
