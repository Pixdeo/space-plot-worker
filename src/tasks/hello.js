import c from 'chalk'

// This task is just an example task

export async function helloTask(job) {
  console.log(`${c.yellow('job:')} Hello! job ${job.id}`)
  await new Promise((r) => setTimeout(r, 2000))
  return { path: 'https://example.com/this/is/is/an/example.plot' }
}
