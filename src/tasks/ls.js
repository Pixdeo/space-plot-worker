import { spawn } from 'child_process'

// Example Task to list dir
export async function lsTask(dir) {
  const ls = spawn('ls')

  ls.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`)
  })
}

// import spawn from 'await-spawn'

// export async function lsTask(dir) {
//   const bl = await spawn('ls')

//   console.log(bl.toString())

// }
