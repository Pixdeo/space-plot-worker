import { DEBUG } from '../worker-config.js'

export function logger(level, args) {
  if (DEBUG >= level) {
    console.log(args)
  }
}
