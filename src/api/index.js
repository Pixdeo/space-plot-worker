import { logger } from '../logger.js'
import fetch from 'node-fetch'

import { host, downloadPath } from '../../worker-config.js'

const base = `${host}/api/jobs`

console.log('api is at:', base)

// EventSource
import EventSource from 'eventsource'

/**
 * Request new job plots, each job is 1 plot
 * @param {number} batchSize Job amount to get
 * @returns {Array} of jobs
 */
export async function fetchJobs(batchSize = 1) {
  // console.info(`Query jobs upto ${c.green(batchSize)} jobs`)
  try {
    const res = await fetch(`${base}/fetch/?batchSize=${batchSize}`)
    const jobs = await res.json()
    return jobs
  } catch (e) {
    console.log(e)
  }
}


export async function statusJob(id, progress, phase, eta) {
  let body = JSON.stringify({ id, progress, phase, eta })
  logger(10, `Server status update - id: ${id}, progress: ${progress}, phase: ${phase}`)
  logger(20, body)
  try {
    const res = await fetch(`${base}/status`, {
      method: 'post',
      body: body,
      headers: { 'Content-Type': 'application/json' }
    })

    return res
  } catch (e) {
    console.log(e)
  }
}

/**
 * Inform the system that this job has been completed
 * @param {string} id of completed job
 * @param {string} url to download the plot
 * @returns
 */
export async function completeJob(id, filePath) {
  let url = downloadPath + filePath
  console.log("downloadPath: " + downloadPath + " filePath: " + filePath )
  console.log(`Completed job ${id}, download url is '${url}'`)
  try {
    const res = await fetch(`${base}/complete`, {
      method: 'post',
      body: JSON.stringify({ id, url }),
      headers: { 'Content-Type': 'application/json' }
    })

    return res
  } catch (e) {
    console.log(e)
  }
}

/**
 *
 * @param {function} onJobs what to execute if more jobs are available
 */
export async function subscribeJobs(onJobs) {
  let url = `${base}/subscribe`
  const sse = new EventSource(url)
  console.log("Subscribed to " + url)
  sse.addEventListener('message', (e) => {
    const data = JSON.parse(e.data)
    // console.log(data.message)
    onJobs(data)
  })

  sse.onerror = function(err) {
    console.error("EventSource error:", err);
  };
}
