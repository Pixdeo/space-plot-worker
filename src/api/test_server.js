import { createServer } from 'http'

createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end();
  const { method, url } = req;
  let body = []
  req.on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
    body = Buffer.concat(body).toString();
    console.log(`${method} ${url} ${body}`)
    // at this point, `body` has the entire request body stored in it as a string
  });
}).listen(3000);
