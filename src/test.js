// adhoc testing
import _ from 'lodash'
import { makePlotConfig, configToParams, mockPlot, createPlot } from './tasks/createPlot/index.js'
import { multibar } from './mutibar.js'
// API endpoints
import { completeJob, fetchJobs, subscribeJobs } from './api/index.js'
import { logger } from './logger.js'

// Which tests to enable
const enableMockTest = 1
const enablePlotTest = 1
const enableMadMaxTest = 0
const enableSubscribeTest = 0

const config = makePlotConfig({kSize:25, tempDir:"/dev/shm/A"})
const plotMadMax = makePlotConfig({plotter: 'madmax', threads: 24, tempDir: '/data/tmp/plots/E/', temp2Dir: '/data/plots/tmp/F/', finalDir: '/data/plots'})
const f = '8ab8695bec66eb09d878981904b84aa5bff9c377ed22d35334a8a678ee7f0d981d28f6610017876ead180eb5d792dbb8'
const p = 'a9b841aad44a64f2020ed525d67841059834adcf91ecec9a61ed6d8f7dbbe240e21fe0066059ff19d532facc5806fee6'

const pending = []
var total = 10
var concurrency = 4
var completedCount = 0

function myPlot(pending, id) {
  let plot = createPlot ({
    jobId: id,
    plotConfig: config,
    poolKey: p,
    farmerKey: f
  }).then(
    (value) => {
      _.remove(pending, p => p === plot)
      completedCount++
      completeJob(id, value)
      console.log(value)
    },
    () => {_.remove(pending, p => p === plot)}
  )
  pending.push(plot)
  return plot
}

function myMockPlot(pending, id) {
  let plot = mockPlot ({
    jobId: id,
    plotConfig: config,
    poolKey: p,
    farmerKey: f
  }).then(
    (value) => {
      _.remove(pending, p => p === plot)
      completedCount++
      completeJob(id, value)
      console.log(value)
    },
    () => {_.remove(pending, p => p === plot)}
  )
  pending.push(plot)
  return plot
}

async function queuePlots(plotFunc) {
  let i = 0
  while (completedCount + pending.length < total) {
    if (pending.length < concurrency) {
      i++
      // fire up a mockPlot
      plotFunc(pending, i)
      console.log("completed: " + completedCount)
      console.log("in progress: " + pending.length)
      await new Promise(resolve => setTimeout(resolve, 500))
    } else {
      await new Promise(resolve => setTimeout(resolve, 500))
    }
  }
}

async function myWait() {
  while (pending.length > 0) {
    await Promise.any(pending)
    await new Promise(resolve => setTimeout(resolve, 500))
  }
  multibar.stop()
  console.log("Completed count: " + completedCount)
}

console.log("config: ")
console.log(configToParams(config))

if (enableMockTest) {
  // test with mock plots first
  console.log("set for total tasks: " + total + ", concurrency: " + concurrency )
  console.log("starting to launch mock jobs now.")
  await new Promise(resolve => setTimeout(resolve, 2000))
  queuePlots(myMockPlot)

  await myWait()
}

if (enablePlotTest) {
  // adjust parameters and test with k25 plots
  total = 4
  concurrency = 2
  completedCount = 0 // reset for new batch
  console.log("set for total tasks: " + total + ", concurrency: " + concurrency )
  console.log("starting to launch k25 jobs now.")
  await new Promise(resolve => setTimeout(resolve, 2000))
  queuePlots(myPlot)

  await myWait()
}

if (enableMadMaxTest) {
  console.log("madmax config: ")
  console.log(configToParams(plotMadMax))

  console.log("launching madmax job now.")
  await new Promise(resolve => setTimeout(resolve, 2000))
  let plot = await createPlot ({
    jobId: 1,
    plotConfig: plotMadMax,
    poolKey: p,
    farmerKey: f
  })

  completeJob(1, plot)
  console.log ("madmax plot completed: " + plot)
  multibar.stop()
}

if (enableSubscribeTest) {
  logger(0,"subscribe test")

  subscribeJobs(async (data) => {
    console.log(data.message)
  })
}
