#!/usr/bin/env node
import { logger } from './logger.js'

// highlight important stuff
import chalk from 'chalk'

// visualization of plot progrss in cli
import { multibar } from './mutibar.js'

// API endpoints
import { completeJob, fetchJobs, subscribeJobs } from './api/index.js'

// Use Configuration
// please check worker-config.js
import { workerLines, maxJobsConcurrency, key } from '../worker-config.js'

// createPlot is responsible for calling *chia plots create exec*
import { createPlot } from './tasks/createPlot/index.js'
// import { plotCreate } from './tasks/plotCreate/log.js'

// Jobs state
import { jobCount } from './State.js'

// Spinner visual hint to know the worker is idle waiting for jobs
import { Spinner } from 'cli-spinner'
Spinner.setDefaultSpinnerDelay(1000)
Spinner.setDefaultSpinnerString(18)
var spinner = new Spinner('Waiting for new Jobs. %s')
var completedCount = 0
var jobNotification = false
var gracefulShutdownFlag = false
// space Worker
const spaceWorkerAppName = chalk.greenBright('[Space Worker]')

console.log(`${spaceWorkerAppName} ${chalk.green(key)}`)
console.log(
  `${spaceWorkerAppName} Max parallel jobs: ${chalk.green(maxJobsConcurrency)}`
)

// create a plot and call the API endpoint when it's finished
async function doJob(job, worker) {
  console.log(`Fetched ${job.id}`)

  const url = await createPlot({
    jobId: job.id,
    plotConfig: worker.plotConfig,
    poolKey: job.data.poolKey,
    farmerKey: job.data.farmerKey
  })

  completeJob(job.id, url)
}

process.on('SIGINT', function () {
  console.log('Caught interrupt signal')
  console.log(jobCount + " currently running jobs")
  console.log(completedCount + " completed jobs")
  // chiaPlot.kill(9)

  multibar.stop()
  process.exit()
})

process.on('SIGUSR1', function () {
  console.log('Caught USR1 - initiating graceful shutdown')
  console.log(jobCount + " currently running jobs")
  console.log(completedCount + " completed jobs")

  // set gracefulShutdownFlag
  gracefulShutdownFlag = true
})

async function gracefulShutdown () {
  // sleep until all jobs completed, then exit
  while (jobCount > 0) {
    console.log("Graceful Shutdown - waiting for " + jobCount + " job(s) to complete..")
    await new Promise(resolve => setTimeout(resolve, 60000))
  }

  console.log("Graceful Shutdown - all jobs completed, exiting.")
  multibar.stop()
  process.exit()
}

function findAvailableWorker() {
  for (let worker of workerLines) {
    if (!('active' in worker) || (worker.active == 0)) {
      return worker
    }
  }
  console.log ("Error, couldn't find an available worker.  This shouldn't happen.")
  return null // shouldn't happen
}

async function waitforNotification() {
  logger(10, "Beginning waitforNotification")
  // reset jobNotification and wait for notification from subscription endpoint
  jobNotification = false
  while (!jobNotification && !gracefulShutdownFlag) {
    // only show spinner if no jobs, otherwise show progressbar
    if (jobCount == 0 && !spinner.isSpinning()) {
      spinner.start()
    }
    await new Promise(resolve => setTimeout(resolve, 1000)) // 1s for now
  }
  // if we got here, jobNotification is true
  spinner.stop()
  console.log("")
  if (jobNotification) {
    logger(10, "Notification recieved, resuming job processing")
  }
  if (gracefulShutdownFlag) {
    logger(10, "Graceful Shutdown recieved, stop waiting for jobs")
  }
  return
}

async function runAgent() {
  while (!gracefulShutdownFlag) {
    if (jobCount < maxJobsConcurrency) {
      const job = await fetchJobs(1) // logic is simpler to fetch one at a time

      if (job == null) {
        // wait for subscription endpoint to notify
        await waitforNotification()
      } else {
        // we got a job, launch it
        console.log("job found: " + JSON.stringify(job[0]))

        // find available worker line
        let worker = findAvailableWorker()

        if (worker != null) {
          // increment and mark active synchronously
          jobCount.increment()
          worker.active = 1
          doJob(job[0], worker).then(
            () => {
              // decrement job count and set worker inactive
              completedCount++
              jobCount.decrement()
              worker.active = 0
            }
          ) // end of doJob.then()
        }
      } // end of if/else on (job == null)
    } else {
      // we're at max concurrency so sleep
      await new Promise(resolve => setTimeout(resolve, 30000)) // 30s sleep
    }
  }

  // if we got here, then we've received the graceful shutdown signal
  gracefulShutdown()
}

// Connect to subscription endpoint
subscribeJobs(async (data) => {
  jobNotification = true
  logger(10, data.message)
})

// Start the program, check Server for plots
runAgent()
